//
// Created by Juan Bernardo Gómez Mendoza on 11/17/23.
//

#include <stdint-gcc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int fbfd;
extern char *fbp;

void pintarImagenEnDispositivo(char* archivo, uint8_t* ptr,
                               int ancho, int alto, int bpp) {
    memcpy(fbp, ptr, ancho*alto*bpp);
}
