#include "main.h"

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>

uint8_t *imagen;
pthread_mutex_t mutex;
int fbfd = 0;
char *fbp = NULL;
pthread_t dibujado, pintado;

int WIDTH, HEIGHT, BPP;

// Función que usa el hilo de dibujado.
void* dibujadoEnMemoria(void*) {
    static uint8_t* buffer = NULL;
    if (buffer == NULL)
        buffer = (uint8_t *)malloc(WIDTH*HEIGHT*BPP);
    if (buffer == NULL) exit(1);
    while(1) {
        dibujarAnimacion(buffer, WIDTH, HEIGHT, BPP);
        pthread_mutex_lock(&mutex);
        memcpy(imagen, buffer, WIDTH*HEIGHT*BPP);
        pthread_mutex_unlock(&mutex);
    }
    free(buffer);  // Nunca debería llegar acá.
}

// Función que usa el hilo de copiado al framebuffer.
void* pintadoEnPantalla(void *) {
    static int cuadrosPerdidos = 0;
    static uint8_t* buffer = NULL;
    if (buffer == NULL)
        buffer = (uint8_t *)malloc(WIDTH*HEIGHT*BPP);
    if (buffer == NULL) exit(1);
    while (1) {
        // El mutex controla el acceso a la imagen compartida.
        if (pthread_mutex_trylock(&mutex) == 0) {
            // Proceso de copia.
            memcpy(buffer, imagen, WIDTH*HEIGHT*BPP);
            pthread_mutex_unlock(&mutex);
        } else {
            cuadrosPerdidos++;
            printf("[I] %d cuadros perdidos.\n\r", cuadrosPerdidos);
        }
        pintarImagenEnDispositivo("/dev/fb0", buffer, WIDTH,
                                  HEIGHT, BPP);
    }
    free(buffer);
}


int main() {
    imagen = NULL;
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    long int screensize = 0;
    int x = 0, y = 0;
    long int location = 0;

    // Open the file for reading and writing
    fbfd = open("/dev/fb0", O_RDWR);
    if (fbfd == -1) {
        perror("Error: cannot open framebuffer device");
        exit(1);
    }

    // Get fixed screen information
    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo) == -1) {
        perror("[E] Error leyendo la información fija.\n\r");
        exit(2);
    }

    // Get variable screen information
    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo) == -1) {
        perror("[E] Error leyendo la información variable.\n\r");
        exit(3);
    }
    WIDTH = vinfo.xres;
    HEIGHT = vinfo.yres;
    BPP = vinfo.bits_per_pixel>>3;
    // Se mapea el framebuffer a la memoria.
    fbp = (char *)mmap(0, WIDTH*HEIGHT*BPP, PROT_READ | PROT_WRITE, MAP_SHARED,
                       fbfd, 0);

    imagen = (uint8_t*)malloc(WIDTH*HEIGHT*BPP);
    if (imagen == NULL) exit(1);
    // Inicialización del mutex.
    pthread_mutex_init(&mutex, NULL);
    // Inicialización de las tareas.
    pthread_create(&dibujado, NULL, dibujadoEnMemoria,
                   NULL);
    pthread_create(&pintado, NULL, pintadoEnPantalla,
                   NULL);
    // Espera a la finalización de los procesos.
    pthread_join(dibujado, NULL);
    pthread_join(pintado, NULL);
    pthread_mutex_destroy(&mutex);
    munmap(fbp, WIDTH*HEIGHT*BPP);
    close(fbfd);
    return 0;
}
