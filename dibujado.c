//
// Created by Juan Bernardo Gómez Mendoza on 11/17/23.
//

#include <stdio.h>
#include "main.h"

int min(int a, int b) { return (a>b)? a: b; }

typedef struct {
    int coord_x;
    int coord_y;
} Coordenada;

int estaEnCirculo(const Coordenada* test,
                  const Coordenada* centro,
                  int radio);
int estaEnCuadrado(const Coordenada* test,
                   const Coordenada* sup_izq,
                   const Coordenada* inf_der);
int inicializarCuadrado(Coordenada*, Coordenada*, int, int);
int incrementarCentro(Coordenada*, int, int);
int incrementarCuadrado(Coordenada*, Coordenada*, int, int);

void dibujarAnimacion(uint8_t* ptr, int ancho, int alto, int bpp) {
    static Coordenada centro_circ = { 0, 0};
    int radio = min(ancho, alto)/8;
    static Coordenada sup_izq = {0,0};
    static Coordenada inf_der = {0,0};
    if (sup_izq.coord_y == inf_der.coord_y ||
        sup_izq.coord_x == inf_der.coord_x)
            inicializarCuadrado(&sup_izq, &inf_der, ancho, alto);
    int idx;
    Coordenada test;
    uint8_t *rgb;
    // Se incrementan las posiciones de la animación.
    incrementarCentro(&centro_circ, ancho, alto);
    incrementarCuadrado(&sup_izq, &inf_der, ancho, alto);
    for (int col=0; col<ancho; ++col)
        for (int fila=0; fila<alto; ++fila) {
            idx = fila*ancho + col;
            rgb = ptr + bpp*idx;  // Puntero al pixel actual
            test.coord_x = col;
            test.coord_y = fila;
            rgb[0] = 0;
            if (bpp==3 || bpp==4) { rgb[1] = 0; rgb[2] = 0; } // Limpia pantalla.
            // El círculo se pinta debajo del cuadrado.
            if (estaEnCirculo(&test, &centro_circ, radio)) {
                if (bpp==3 || bpp==4) {
                    rgb[0] = 255; rgb[1] = 255; rgb[2] = 0; // Amarillo.
                }
                if (bpp==1) rgb[0]==192; // Gris claro.
            }
            // Y el cuadrado se pinta encima.
            if (estaEnCuadrado(&test, &sup_izq, &inf_der)) {
                if (bpp==3 || bpp==4) {
                    rgb[0] = 64; rgb[1] = 64; rgb[2] = 255; // Azul.
                }
                if (bpp==1) rgb[0]==96; // Gris oscuro.
            }
        }
}

int estaEnCirculo(const Coordenada* test, const Coordenada* centro,
                  int radio) {
    int dX = test->coord_x - centro->coord_x;
    int dY = test->coord_y - centro->coord_y;
    int dist2 = dX*dX + dY*dY;
    if (dist2<=radio*radio) return 1;
    return 0;
}

int estaEnCuadrado(const Coordenada* test,
                   const Coordenada* sup_izq,
                   const Coordenada* inf_der) {
    if (test->coord_x>=sup_izq->coord_x &&
        test->coord_y>=sup_izq->coord_y &&
        test->coord_x<=inf_der->coord_x &&
        test->coord_y<=inf_der->coord_y) return 1;
    return 0;
}

int inicializarCuadrado(Coordenada* sup_izq, Coordenada* inf_der,
                        int ancho, int alto) {
    int lado = min(ancho, alto)/5;
    sup_izq->coord_x=-lado/2;  sup_izq->coord_y=(alto-lado)/2;
    inf_der->coord_x=sup_izq->coord_x+lado;
    inf_der->coord_y=sup_izq->coord_y+lado;
}

int incrementarCentro(Coordenada* centro, int ancho, int alto) {
    static int dx = 5;
    static int dy = 3;
    if (centro->coord_x > ancho-5) dx = -5;
    if (centro->coord_x < 5) dx = 5;
    if (centro->coord_y > alto-3) dy = -3;
    if (centro->coord_y < 3) dy = 3;
    centro->coord_x += dx;
    centro->coord_y += dy;
}

int incrementarCuadrado(Coordenada* sup_izq, Coordenada* inf_der,
                        int ancho, int alto) {
    static int dx = 3;
    static int dy = 5;
    int lado = inf_der->coord_x - sup_izq->coord_x;
    Coordenada centro = {sup_izq->coord_x + lado/2,
                         sup_izq->coord_y + lado/2};
    if (centro.coord_x > ancho-3) dx = -3;
    if (centro.coord_x < 3) dx = 3;
    if (centro.coord_y > alto-5) dy = -5;
    if (centro.coord_y < 5) dy = 5;
    sup_izq->coord_x += dx; inf_der->coord_x += dx;
    sup_izq->coord_y += dy; inf_der->coord_y += dy;
}


