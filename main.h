//
// Created by Juan Bernardo Gómez Mendoza on 11/17/23.
//

#ifndef BUFFERDOBLE_MAIN_H
#define BUFFERDOBLE_MAIN_H

#include <stdint-gcc.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <signal.h>

void dibujarAnimacion(uint8_t* , int, int, int);
void pintarImagenEnDispositivo(char* , uint8_t* , int, int, int);

#endif //BUFFERDOBLE_MAIN_H
